// Made by Singh, Rebabre 2019 Mar

/*
 *  Location 18 X 13 map of output
 *
 *  1l1 1l2 1l3 1l4 1l5 1l6 1l7 1l8  1l9 1l10 1l11 1l12 1l131l141l151l161l171l18
 *  2l1  12  13  14  15  16  17  18  19  110 111 112  113 114 115 116 117 2l18
 *  3l1  12  13  14  15  16  17  18  19  110 111 112  113 114 115 116 117 3l18
 *  4l1  12  13  14  15  16  17  18  19  110 111 112  113 114 115 116 117 4l18
 *  5l1  12  13  14  15  16  17  18  19  110 111 112  113 114 115 116 117 5l18
 *  l6l1  12  13  14  15  16  17  18  19  110 111 112  113 114 115 116 117 6l18
 *  7l1  12  13  14  15  16  17  18  19  110 111 112  113 114 115 116 117 7l18
 *  8l1  12  13  14  15  16  17  18  19  110 111 112  113 114 115 116 117 8l18
 *  9l1  12  13  14  15  16  17  18  19  110 111 112  113 114 115 116 117 9l18
 *  10l1  12  13  14  15  16  17  18  19  110 111 112  113 114 115 116 117 1018
 *  11l1  12  13  14  15  16  17  18  19  110 111 112  113 114 115 116 117 1118
 *  12l1  12  13  14  15  16  17  18  19  110 111 112  113 114 115 116 117 1218
 *  13l1  12  13  14  15  16  17  18  19  110 111 112  113 114 115 116 117 1318
 *
 */

import React from 'react';
import ReactDOM from 'react-dom';

let posManip = '';
let posCorids = '';
let nexPos = '';

class HelloWorld extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      di: 'w', // direction
      startPos: 'l7l2', // direction
      snakeBody: ['l7l2'],
      l1l1: 0, // top left
      l1l2: 0,
      l1l3: 0,
      l1l4: 0,
      l1l5: 0,
      l1l6: 0,
      l1l7: 0,
      l1l8: 0,
      l1l9: 0,
      l1l10: 0,
      l1l11: 0,
      l1l12: 0,
      l1l13: 0,
      l1l14: 0,
      l1l15: 0,
      l1l16: 0,
      l1l17: 0,
      l1l18: 0,
      l2l1: 0,
      l2l2: 0,
      l2l3: 0,
      l2l4: 0,
      l2l5: 0,
      l2l6: 0,
      l2l7: 0,
      l2l8: 0,
      l2l9: 0,
      l2l10: 0,
      l2l11: 0,
      l2l12: 0,
      l2l13: 0,
      l2l14: 0,
      l2l15: 0,
      l2l16: 0,
      l2l17: 0,
      l2l18: 0,
      l3l1: 0,
      l3l2: 0,
      l3l3: 0,
      l3l4: 0,
      l3l5: 0,
      l3l6: 0,
      l3l7: 0,
      l3l8: 0,
      l3l9: 0,
      l3l10: 0,
      l3l11: 0,
      l3l12: 0,
      l3l13: 0,
      l3l14: 0,
      l3l15: 0,
      l3l16: 0,
      l3l17: 0,
      l3l18: 0,
      l4l1: 0,
      l4l2: 0,
      l4l3: 0,
      l4l4: 0,
      l4l5: 0,
      l4l6: 0,
      l4l7: 0,
      l4l8: 0,
      l4l9: 0,
      l4l10: 0,
      l4l11: 0,
      l4l12: 0,
      l4l13: 0,
      l4l14: 0,
      l4l15: 0,
      l4l16: 0,
      l4l17: 0,
      l4l18: 0,
      l5l1: 0,
      l5l2: 0,
      l5l3: 0,
      l5l4: 0,
      l5l5: 0,
      l5l6: 0,
      l5l7: 0,
      l5l8: 0,
      l5l9: 0,
      l5l10: 0,
      l5l11: 0,
      l5l12: 0,
      l5l13: 0,
      l5l14: 0,
      l5l15: 0,
      l5l16: 0,
      l5l17: 0,
      l5l18: 0,
      l6l1: 0,
      l6l2: 0,
      l6l3: 0,
      l6l4: 0,
      l6l5: 0,
      l6l6: 0,
      l6l7: 0,
      l6l8: 0,
      l6l9: 0,
      l6l10: 0,
      l6l11: 0,
      l6l12: 0,
      l6l13: 0,
      l6l14: 0,
      l6l15: 0,
      l6l16: 0,
      l6l17: 0,
      l6l18: 0,
      l7l1: 0,
      l7l2: 6,
      l7l3: 0,
      l7l4: 0,
      l7l5: 0,
      l7l6: 0,
      l7l7: 0,
      l7l8: 0,
      l7l9: 0,
      l7l10: 0,
      l7l11: 0,
      l7l12: 0,
      l7l13: 0,
      l7l14: 0,
      l7l15: 0,
      l7l16: 0,
      l7l17: 0,
      l7l18: 0,
      l8l1: 0,
      l8l2: 0,
      l8l3: 0,
      l8l4: 0,
      l8l5: 0,
      l8l6: 0,
      l8l7: 0,
      l8l8: 0,
      l8l9: 0,
      l8l10: 0,
      l8l11: 0,
      l8l12: 0,
      l8l13: 0,
      l8l14: 0,
      l8l15: 0,
      l8l16: 0,
      l8l17: 0,
      l8l18: 0,
      l9l1: 0,
      l9l2: 0,
      l9l3: 0,
      l9l4: 0,
      l9l5: 0,
      l9l6: 0,
      l9l7: 0,
      l9l8: 0,
      l9l9: 0,
      l9l10: 0,
      l9l11: 0,
      l9l12: 0,
      l9l13: 0,
      l9l14: 0,
      l9l15: 0,
      l9l16: 0,
      l9l17: 0,
      l9l18: 0,
      l10l1: 0,
      l10l2: 0,
      l10l3: 0,
      l10l4: 0,
      l10l5: 0,
      l10l6: 0,
      l10l7: 0,
      l10l8: 0,
      l10l9: 0,
      l10l10: 0,
      l10l11: 0,
      l10l12: 0,
      l10l13: 0,
      l10l14: 0,
      l10l15: 0,
      l10l16: 0,
      l10l17: 0,
      l10l18: 0,
      l11l1: 0,
      l11l2: 0,
      l11l3: 0,
      l11l4: 0,
      l11l5: 0,
      l11l6: 0,
      l11l7: 0,
      l11l8: 0,
      l11l9: 0,
      l11l10: 0,
      l11l11: 0,
      l11l12: 0,
      l11l13: 0,
      l11l14: 0,
      l11l15: 0,
      l11l16: 0,
      l11l17: 0,
      l11l18: 0,
      l12l1: 0,
      l12l2: 0,
      l12l3: 0,
      l12l4: 0,
      l12l5: 0,
      l12l6: 0,
      l12l7: 0,
      l12l8: 0,
      l12l9: 0,
      l12l10: 0,
      l12l11: 0,
      l12l12: 0,
      l12l13: 0,
      l12l14: 0,
      l12l15: 0,
      l12l16: 0,
      l12l17: 0,
      l12l18: 0,
      l13l1: 0,
      l13l2: 0,
      l13l3: 0,
      l13l4: 0,
      l13l5: 0,
      l13l6: 0,
      l13l7: 0,
      l13l8: 0,
      l13l9: 0,
      l13l10: 0,
      l13l11: 0,
      l13l12: 0,
      l13l13: 0,
      l13l14: 0,
      l13l15: 0,
      l13l16: 0,
      l13l17: 0,
      l13l18: 0, // bottom right
    };
    this.move = this.move.bind(this);
    this.time = this.time.bind(this);
    this.handleKeyPress = this.handleKeyPress.bind(this);
  }

  componentDidMount() {
    this.time();
    document.addEventListener('keydown', this.handleKeyPress, false);
  }

  handleKeyPress = (event) => {
    /* eslint-disable no-console */
    console.log(`Key pressed : ${event.key.toUpperCase()}`);
    if (event.key.toUpperCase() === 'ARROWUP') {
      this.setState({
        di: 'n',
      });
    } else if (event.key.toUpperCase() === 'ARROWDOWN') {
      this.setState({
        di: 's',
      });
    } else if (event.key.toUpperCase() === 'ARROWLEFT') {
      this.setState({
        di: 'e',
      });
    } else if (event.key.toUpperCase() === 'ARROWRIGHT') {
      this.setState({
        di: 'w',
      });
    }
  };

  time() {
    setInterval(
      () => this.move(), 1000,
    );
  }

  move() {
    // d for dead
    if (this.state.di !== 'd') {
      /* eslint-disable no-console */
      // console.log(this.state.startPos);
      posManip = this.state.startPos;
      posCorids = posManip.split('l');
      /* eslint-disable no-console */
      // console.log(posCorids);

      // Go one
      if (this.state.di === 'w') {
        const nextOne = parseInt(posCorids[2], 10) + 1;
        nexPos = `l${posCorids[1]}l${nextOne}`;
      }

      if (this.state.di === 'n') {
        const nextOne = parseInt(posCorids[1], 10) - 1;
        nexPos = `l${nextOne}l${posCorids[2]}`;
      }

      if (this.state.di === 'e') {
        const nextOne = parseInt(posCorids[2], 10) - 1;
        nexPos = `l${posCorids[1]}l${nextOne}`;
      }

      if (this.state.di === 's') {
        const nextOne = parseInt(posCorids[1], 10) + 1;
        nexPos = `l${nextOne}l${posCorids[2]}`;
      }
    }

    // Decrease tail all
    /* eslint-disable no-console */
    console.log(this.state.snakeBody);
    const joined = this.state.snakeBody.concat(nexPos);

    // loop through each on
    joined.forEach((item, i) => {
      if (parseInt(this.state[`${joined[i]}`], 10) !== 0) {
        this.setState({
          [`${joined[i]}`]: (parseInt(this.state[`${joined[i]}`], 10) - 1),
        });
      }
    });

    this.setState({ snakeBody: joined });

    this.setState({
      startPos: nexPos, // direction
      [`${nexPos}`]: '6',
    });
  }

  render() {
    return (
      <div>
        <div id="wrapper">
          <div>
            <span id="text">{this.state.l1l1}</span>
            <span id="text">{this.state.l1l2}</span>
            <span id="text">{this.state.l1l3}</span>
            <span id="text">{this.state.l1l4}</span>
            <span id="text">{this.state.l1l5}</span>
            <span id="text">{this.state.l1l6}</span>
            <span id="text">{this.state.l1l7}</span>
            <span id="text">{this.state.l1l8}</span>
            <span id="text">{this.state.l1l9}</span>
            <span id="text">{this.state.l1l10}</span>
            <span id="text">{this.state.l1l11}</span>
            <span id="text">{this.state.l1l12}</span>
            <span id="text">{this.state.l1l13}</span>
            <span id="text">{this.state.l1l14}</span>
            <span id="text">{this.state.l1l15}</span>
            <span id="text">{this.state.l1l16}</span>
            <span id="text">{this.state.l1l17}</span>
            <span id="text">{this.state.l1l18}</span>
          </div>

          <div>
            <span id="text">{this.state.l2l1}</span>
            <span id="text">{this.state.l2l2}</span>
            <span id="text">{this.state.l2l3}</span>
            <span id="text">{this.state.l2l4}</span>
            <span id="text">{this.state.l2l5}</span>
            <span id="text">{this.state.l2l6}</span>
            <span id="text">{this.state.l2l7}</span>
            <span id="text">{this.state.l2l8}</span>
            <span id="text">{this.state.l2l9}</span>
            <span id="text">{this.state.l2l10}</span>
            <span id="text">{this.state.l2l11}</span>
            <span id="text">{this.state.l2l12}</span>
            <span id="text">{this.state.l2l13}</span>
            <span id="text">{this.state.l2l14}</span>
            <span id="text">{this.state.l2l15}</span>
            <span id="text">{this.state.l2l16}</span>
            <span id="text">{this.state.l2l17}</span>
            <span id="text">{this.state.l2l18}</span>
          </div>

          <div>
            <span id="text">{this.state.l3l1}</span>
            <span id="text">{this.state.l3l2}</span>
            <span id="text">{this.state.l3l3}</span>
            <span id="text">{this.state.l3l4}</span>
            <span id="text">{this.state.l3l5}</span>
            <span id="text">{this.state.l3l6}</span>
            <span id="text">{this.state.l3l7}</span>
            <span id="text">{this.state.l3l8}</span>
            <span id="text">{this.state.l3l9}</span>
            <span id="text">{this.state.l3l10}</span>
            <span id="text">{this.state.l3l11}</span>
            <span id="text">{this.state.l3l12}</span>
            <span id="text">{this.state.l3l13}</span>
            <span id="text">{this.state.l3l14}</span>
            <span id="text">{this.state.l3l15}</span>
            <span id="text">{this.state.l3l16}</span>
            <span id="text">{this.state.l3l17}</span>
            <span id="text">{this.state.l3l18}</span>
          </div>

          <div>
            <span id="text">{this.state.l4l1}</span>
            <span id="text">{this.state.l4l2}</span>
            <span id="text">{this.state.l4l3}</span>
            <span id="text">{this.state.l4l4}</span>
            <span id="text">{this.state.l4l5}</span>
            <span id="text">{this.state.l4l6}</span>
            <span id="text">{this.state.l4l7}</span>
            <span id="text">{this.state.l4l8}</span>
            <span id="text">{this.state.l4l9}</span>
            <span id="text">{this.state.l4l10}</span>
            <span id="text">{this.state.l4l11}</span>
            <span id="text">{this.state.l4l12}</span>
            <span id="text">{this.state.l4l13}</span>
            <span id="text">{this.state.l4l14}</span>
            <span id="text">{this.state.l4l15}</span>
            <span id="text">{this.state.l4l16}</span>
            <span id="text">{this.state.l4l17}</span>
            <span id="text">{this.state.l4l18}</span>
          </div>

          <div>
            <span id="text">{this.state.l5l1}</span>
            <span id="text">{this.state.l5l2}</span>
            <span id="text">{this.state.l5l3}</span>
            <span id="text">{this.state.l5l4}</span>
            <span id="text">{this.state.l5l5}</span>
            <span id="text">{this.state.l5l6}</span>
            <span id="text">{this.state.l5l7}</span>
            <span id="text">{this.state.l5l8}</span>
            <span id="text">{this.state.l5l9}</span>
            <span id="text">{this.state.l5l10}</span>
            <span id="text">{this.state.l5l11}</span>
            <span id="text">{this.state.l5l12}</span>
            <span id="text">{this.state.l5l13}</span>
            <span id="text">{this.state.l5l14}</span>
            <span id="text">{this.state.l5l15}</span>
            <span id="text">{this.state.l5l16}</span>
            <span id="text">{this.state.l5l17}</span>
            <span id="text">{this.state.l5l18}</span>
          </div>

          <div>
            <span id="text">{this.state.l6l1}</span>
            <span id="text">{this.state.l6l2}</span>
            <span id="text">{this.state.l6l3}</span>
            <span id="text">{this.state.l6l4}</span>
            <span id="text">{this.state.l6l5}</span>
            <span id="text">{this.state.l6l6}</span>
            <span id="text">{this.state.l6l7}</span>
            <span id="text">{this.state.l6l8}</span>
            <span id="text">{this.state.l6l9}</span>
            <span id="text">{this.state.l6l10}</span>
            <span id="text">{this.state.l6l11}</span>
            <span id="text">{this.state.l6l12}</span>
            <span id="text">{this.state.l6l13}</span>
            <span id="text">{this.state.l6l14}</span>
            <span id="text">{this.state.l6l15}</span>
            <span id="text">{this.state.l6l16}</span>
            <span id="text">{this.state.l6l17}</span>
            <span id="text">{this.state.l6l18}</span>
          </div>

          <div>
            <span id="text">{this.state.l7l1}</span>
            <span id="text">{this.state.l7l2}</span>
            <span id="text">{this.state.l7l3}</span>
            <span id="text">{this.state.l7l4}</span>
            <span id="text">{this.state.l7l5}</span>
            <span id="text">{this.state.l7l6}</span>
            <span id="text">{this.state.l7l7}</span>
            <span id="text">{this.state.l7l8}</span>
            <span id="text">{this.state.l7l9}</span>
            <span id="text">{this.state.l7l10}</span>
            <span id="text">{this.state.l7l11}</span>
            <span id="text">{this.state.l7l12}</span>
            <span id="text">{this.state.l7l13}</span>
            <span id="text">{this.state.l7l14}</span>
            <span id="text">{this.state.l7l15}</span>
            <span id="text">{this.state.l7l16}</span>
            <span id="text">{this.state.l7l17}</span>
            <span id="text">{this.state.l7l18}</span>
          </div>

          <div>
            <span id="text">{this.state.l8l1}</span>
            <span id="text">{this.state.l8l2}</span>
            <span id="text">{this.state.l8l3}</span>
            <span id="text">{this.state.l8l4}</span>
            <span id="text">{this.state.l8l5}</span>
            <span id="text">{this.state.l8l6}</span>
            <span id="text">{this.state.l8l7}</span>
            <span id="text">{this.state.l8l8}</span>
            <span id="text">{this.state.l8l9}</span>
            <span id="text">{this.state.l8l10}</span>
            <span id="text">{this.state.l8l11}</span>
            <span id="text">{this.state.l8l12}</span>
            <span id="text">{this.state.l8l13}</span>
            <span id="text">{this.state.l8l14}</span>
            <span id="text">{this.state.l8l15}</span>
            <span id="text">{this.state.l8l16}</span>
            <span id="text">{this.state.l8l17}</span>
            <span id="text">{this.state.l8l18}</span>
          </div>

          <div>
            <span id="text">{this.state.l9l1}</span>
            <span id="text">{this.state.l9l2}</span>
            <span id="text">{this.state.l9l3}</span>
            <span id="text">{this.state.l9l4}</span>
            <span id="text">{this.state.l9l5}</span>
            <span id="text">{this.state.l9l6}</span>
            <span id="text">{this.state.l9l7}</span>
            <span id="text">{this.state.l9l8}</span>
            <span id="text">{this.state.l9l9}</span>
            <span id="text">{this.state.l9l10}</span>
            <span id="text">{this.state.l9l11}</span>
            <span id="text">{this.state.l9l12}</span>
            <span id="text">{this.state.l9l13}</span>
            <span id="text">{this.state.l9l14}</span>
            <span id="text">{this.state.l9l15}</span>
            <span id="text">{this.state.l9l16}</span>
            <span id="text">{this.state.l9l17}</span>
            <span id="text">{this.state.l9l18}</span>
          </div>

          <div>
            <span id="text">{this.state.l10l1}</span>
            <span id="text">{this.state.l10l2}</span>
            <span id="text">{this.state.l10l3}</span>
            <span id="text">{this.state.l10l4}</span>
            <span id="text">{this.state.l10l5}</span>
            <span id="text">{this.state.l10l6}</span>
            <span id="text">{this.state.l10l7}</span>
            <span id="text">{this.state.l10l8}</span>
            <span id="text">{this.state.l10l9}</span>
            <span id="text">{this.state.l10l10}</span>
            <span id="text">{this.state.l10l11}</span>
            <span id="text">{this.state.l10l12}</span>
            <span id="text">{this.state.l10l13}</span>
            <span id="text">{this.state.l10l14}</span>
            <span id="text">{this.state.l10l15}</span>
            <span id="text">{this.state.l10l16}</span>
            <span id="text">{this.state.l10l17}</span>
            <span id="text">{this.state.l10l18}</span>
          </div>

          <div>
            <span id="text">{this.state.l11l1}</span>
            <span id="text">{this.state.l11l2}</span>
            <span id="text">{this.state.l11l3}</span>
            <span id="text">{this.state.l11l4}</span>
            <span id="text">{this.state.l11l5}</span>
            <span id="text">{this.state.l11l6}</span>
            <span id="text">{this.state.l11l7}</span>
            <span id="text">{this.state.l11l8}</span>
            <span id="text">{this.state.l11l9}</span>
            <span id="text">{this.state.l11l10}</span>
            <span id="text">{this.state.l11l11}</span>
            <span id="text">{this.state.l11l12}</span>
            <span id="text">{this.state.l11l13}</span>
            <span id="text">{this.state.l11l14}</span>
            <span id="text">{this.state.l11l15}</span>
            <span id="text">{this.state.l11l16}</span>
            <span id="text">{this.state.l11l17}</span>
            <span id="text">{this.state.l11l18}</span>
          </div>

          <div>
            <span id="text">{this.state.l12l1}</span>
            <span id="text">{this.state.l12l2}</span>
            <span id="text">{this.state.l12l3}</span>
            <span id="text">{this.state.l12l4}</span>
            <span id="text">{this.state.l12l5}</span>
            <span id="text">{this.state.l12l6}</span>
            <span id="text">{this.state.l12l7}</span>
            <span id="text">{this.state.l12l8}</span>
            <span id="text">{this.state.l12l9}</span>
            <span id="text">{this.state.l12l10}</span>
            <span id="text">{this.state.l12l11}</span>
            <span id="text">{this.state.l12l12}</span>
            <span id="text">{this.state.l12l13}</span>
            <span id="text">{this.state.l12l14}</span>
            <span id="text">{this.state.l12l15}</span>
            <span id="text">{this.state.l12l16}</span>
            <span id="text">{this.state.l12l17}</span>
            <span id="text">{this.state.l12l18}</span>
          </div>

          <div>
            <span id="text">{this.state.l13l1}</span>
            <span id="text">{this.state.l13l2}</span>
            <span id="text">{this.state.l13l3}</span>
            <span id="text">{this.state.l13l4}</span>
            <span id="text">{this.state.l13l5}</span>
            <span id="text">{this.state.l13l6}</span>
            <span id="text">{this.state.l13l7}</span>
            <span id="text">{this.state.l13l8}</span>
            <span id="text">{this.state.l13l9}</span>
            <span id="text">{this.state.l13l10}</span>
            <span id="text">{this.state.l13l11}</span>
            <span id="text">{this.state.l13l12}</span>
            <span id="text">{this.state.l13l13}</span>
            <span id="text">{this.state.l13l14}</span>
            <span id="text">{this.state.l13l15}</span>
            <span id="text">{this.state.l13l16}</span>
            <span id="text">{this.state.l13l17}</span>
            <span id="text">{this.state.l13l18}</span>
          </div>
        </div>
      </div>
    );
  }
}

ReactDOM.render(<HelloWorld />, document.getElementById('react'));

/*
*
* Add foreground color (see workflows)
*
*/
